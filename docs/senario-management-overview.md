# Senario 관리 개요

> 코드를 [여기서](https://docs.taipy.io/scenario_management.py) 다운로드 받을 수 있다. 여기 [TOML 파일](https://docs.taipy.io/config.toml)이 있는 [Python 프로그램](https://docs.taipy.io/scenario_management_toml.py)이 있다.

이 튜토리얼이 끝나면 Taipy의 시나리오 관리를 사용하여 작은 어플리케이션을 만들 수 있는 모든 기반을 갖추게 된다.

![](./demo.gif)

일부 코드 예제를 살펴보기 전에 시나리오가 무엇인지 이해하려면 데이터 노드와 작업 개념을 이해해야 한다.

- [**데이터 노드(Data Node)**](https://docs.taipy.io/en/release-3.0/manuals/core/concepts/data-node/): Taipy의 변수를 나타낸다. 데이터 노드는 데이터 자체를 포함하지 않고 데이터를 가리키며 검색하는 방법을 알고 있다. 이러한 데이터 노드는 CSV 파일, 피클(Pickle) 파일, 데이터베이스 등 다양한 유형의 데이터 소스를 가리키며 정수, 문자열, 데이터 프레임, 리스트 등 다양한 유형의 Python 변수를 나타낼 수 있다. 데이터세트, 매개 변수, KPI, 중간 데이터 또는 모든 변수를 나타내는 데 사용할 수 있으며 완전히 일반적이다.
- [**작업(Task)**](https://docs.taipy.io/en/release-3.0/manuals/core/concepts/task/): Taipy에서 함수의 변환으로 생각할 수 있으며, 입력과 출력이 데이터 노드이다.
- [**시나리오(Senario)**](https://docs.taipy.io/en/release-3.0/manuals/core/concepts/scenario/): 데이터 노드와 작업을 결합하여 실행 흐름을 매핑하는 그래프를 구성하여 시나리오를 만든다. 각 시나리오를 제출하면 작업이 실행된다. 최종 사용자는 다양한 비즈니스 상황을 반영하기 위해 다양한 매개 변수를 수정해야 하는 경우가 매우 빈번하다. Taipy는 다양한 상황(즉, 최종 사용자가 설정한 다양한 데이터/파라미터 값)에서 다양한 시나리오를 실행할 수 있는 프레임워크를 제공한다.

[Configuration](https://docs.taipy.io/en/release-3.0/manuals/core/config/)은 시나리오를 정의한 구조이다. Configuration은 Directed Acyclic Graph(들)의 Blueprint 역할을 하며 데이터 소스, 파라미터 및 작업을 모델링한다. Configuration은 정의된 후 Superclass와 같은 역할을 하며 다양한 시나리오 인스턴스를 생성하는 데 사용된다.


