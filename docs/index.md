# Senario 관리 개요

> 코드를 [여기서](https://docs.taipy.io/scenario_management.py) 다운로드 받을 수 있다. 여기 [TOML 파일](https://docs.taipy.io/config.toml)이 있는 [Python 프로그램](https://docs.taipy.io/scenario_management_toml.py)이 있다.

이 튜토리얼이 끝나면 Taipy의 시나리오 관리를 사용하여 작은 어플리케이션을 만들 수 있는 모든 기반을 갖추게 된다.

![](./demo.gif)

일부 코드 예제를 살펴보기 전에 시나리오가 무엇인지 이해하려면 데이터 노드와 작업 개념을 이해해야 한다.

- [**데이터 노드(Data Node)**](https://docs.taipy.io/en/release-3.0/manuals/core/concepts/data-node/): Taipy의 변수를 나타낸다. 데이터 노드는 데이터 자체를 포함하지 않고 데이터를 가리키며 검색하는 방법을 알고 있다. 이러한 데이터 노드는 CSV 파일, 피클(Pickle) 파일, 데이터베이스 등 다양한 유형의 데이터 소스를 가리키며 정수, 문자열, 데이터 프레임, 리스트 등 다양한 유형의 Python 변수를 나타낼 수 있다. 데이터세트, 매개 변수, KPI, 중간 데이터 또는 모든 변수를 나타내는 데 사용할 수 있으며 완전히 일반적이다.
- [**작업(Task)**](https://docs.taipy.io/en/release-3.0/manuals/core/concepts/task/): Taipy에서 함수의 변환으로 생각할 수 있으며, 입력과 출력이 데이터 노드이다.
- [**시나리오(Senario)**](https://docs.taipy.io/en/release-3.0/manuals/core/concepts/scenario/): 데이터 노드와 작업을 결합하여 실행 흐름을 매핑하는 그래프를 구성하여 시나리오를 만든다. 각 시나리오를 제출하면 작업이 실행된다. 최종 사용자는 다양한 비즈니스 상황을 반영하기 위해 다양한 매개 변수를 수정해야 하는 경우가 매우 빈번하다. Taipy는 다양한 상황(즉, 최종 사용자가 설정한 다양한 데이터/파라미터 값)에서 다양한 시나리오를 실행할 수 있는 프레임워크를 제공한다.

[Configuration](https://docs.taipy.io/en/release-3.0/manuals/core/config/)은 시나리오를 정의한 구조이다. Configuration은 Directed Acyclic Graph(들)의 Blueprint 역할을 하며 데이터 소스, 파라미터 및 작업을 모델링한다. Configuration은 정의된 후 Superclass와 같은 역할을 하며 다양한 시나리오 인스턴스를 생성하는 데 사용된다.

## 시나리오 구성
가장 기본적인 파이프라인을 생각해 보자. 작동하는 데 두 가지가 필요한 함수, 즉 일부 데이터와 날짜를 생각해 본다. 이를 사용하여 해당 날짜에 대한 예측을 생성한다.

아래의 이 함수에 대한 코드를 참조하세요.

```python
import pandas as pd

def predict(historical_temperature: pd.DataFrame, date_to_forecast: str) -> float:
    print(f"Running baseline...")
    historical_temperature['Date'] = pd.to_datetime(historical_temperature['Date'])
    historical_same_day = historical_temperature.loc[
        (historical_temperature['Date'].dt.day == date_to_forecast.day) &
        (historical_temperature['Date'].dt.month == date_to_forecast.month)
    ]
    return historical_same_day['Temp'].mean()
```

시나리오는 다음 그래프와 같이 나타낼 수 있다.

![](./config.svg)

3개의 데이터 노드(`historical_temperature`, `date_to_forces`와 `predictions`)를 구성하고 있다. 작업 `predict`는 Pythob 함수를 통해 3개의 데이터 노드를 연결한다.

간단한 시나리오를 구성하기 위한 코드는 다음과 같다.

```python
from taipy import Config

# Configuration of Data Nodes
historical_temperature_cfg = Config.configure_data_node("historical_temperature")
date_to_forecast_cfg = Config.configure_data_node("date_to_forecast")
predictions_cfg = Config.configure_data_node("predictions")

# Configuration of tasks
predict_cfg = Config.configure_task(id="predict",
                                    function=predict,
                                    input=[historical_temperature_cfg, date_to_forecast_cfg],
                                    output=predictions_cfg)

# Configuration of scenario
scenario_cfg = Config.configure_scenario(id="my_scenario", task_configs=[predict_cfg])
```

구성이 완료되었다! 시나리오를 인스턴스화하여 제출하는 데 사용할 수 있다.

## 시나리오 인스턴스화
먼저 Core 서비스를 코드(`tp.Core().run()`)로 실행한다. 그런 다음 Taipy와 함께 실행할 수 있다.

- 시나리오를 작성한다
- 입력 데이터 노드를 작성하고,
- 일을 처리하기 위해 그들을 제출한다
- 출력 데이터 노드를 읽는다.

시나리오를 작성하면(`tp.create_scenario(<Scenario Config>)`), 해당 객체(`task`, `Data Nodes` 등)가 모두 생성된다. 이 객체들은 이전 구성 덕분에 생성된다. 아직까지 실행된 시나리오는 없다. `tp.submit(<Scenario>)`는 모든 시나리오 관련 작업의 실행을 트리거하는 코드이다.

```python
import taipy as tp

# Run of the Core
tp.Core().run()

# Creation of the scenario and execution
scenario = tp.create_scenario(scenario_cfg)
scenario.historical_temperature.write(data)
scenario.date_to_forecast.write(dt.datetime.now())
tp.submit(scenario)

print("Value at the end of task", scenario.predictions.read())
```

출력:

```
[2022-12-22 16:20:02,740][Taipy][INFO] job JOB_predict_... is completed.
Value at the end of task 23.45
```

위의 코드에서는 시나리오 작성과 제출, 데이터 노드 검색, 데이터 읽기와 쓰기를 볼 수 있다. 매뉴얼, 특히 [taipy](https://docs.taipy.io/en/release-3.0/manuals/reference/pkg_taipy/), [senario](https://docs.taipy.io/en/release-3.0/manuals/core/entities/scenario-cycle-mgt/) 및 [data node](https://docs.taipy.io/en/release-3.0/manuals/core/entities/data-node-mgt/) 문서 페이지에는 다른 많은 기능을 설명하고 있다.

## 시각적 요소
앞 절의 작은 코드 조각은 시나리오 관리 방법을 보여준다. 시나리오나 데이터 노드 관리는 일반적으로 그래픽 인터페이스를 통해 최종 사용자가 한다. Taipy는 위의 코드를 대체하기 위해 시나리오 관리 전용 시각적 요소를 제공한다.

스크립트의 코드에 아래 몇 줄을 추가한다. 그러면 최종 사용자들이 다음을 할 수 있다.

- 시나리오 선택하고,
- 새 시나리오을 만들고,
- 그것들을 제출하고,
- 그들의 속성을 액세스한다.

```python
import taipy as tp

def save(state):
    # write values of Data Node to submit scenario
    state.scenario.historical_temperature.write(data)
    state.scenario.date_to_forecast.write(state.date)
    tp.gui.notify(state, "s", "Saved! Ready to submit")

date = None
scenario_md = """
<|{scenario}|scenario_selector|>

Put a Date
<|{date}|date|on_change=save|active={scenario}|>

Run the scenario
<|{scenario}|scenario|>
<|{scenario}|scenario_dag|>

View all the information on your prediction here
<|{scenario.predictions if scenario else None}|data_node|>
"""

tp.Gui(scenario_md).run()
```

[시나리오 관리 컨트롤](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/controls/#scenario-management-controls) http://127.0.0.1:8000/en/develop/manuals/는 시나리오와 데이터 노드를 액세스하고 관리하는 데 필요한 모든 기능을 제공한다. 사실 파이프라인에 연결된 시나리오 기반 어플리케이션을 만드는 것은 그 어느 때보다 간단했다.

![](./demo.gif)

## 전체 코드

```python
from taipy import Config
import taipy as tp
import pandas as pd
import datetime as dt


data = pd.read_csv("https://raw.githubusercontent.com/Avaiga/taipy-getting-started-core/develop/src/daily-min-temperatures.csv")


# Normal function used by Taipy
def predict(historical_temperature: pd.DataFrame, date_to_forecast: str) -> float:
    print(f"Running baseline...")
    historical_temperature['Date'] = pd.to_datetime(historical_temperature['Date'])
    historical_same_day = historical_temperature.loc[
        (historical_temperature['Date'].dt.day == date_to_forecast.day) &
        (historical_temperature['Date'].dt.month == date_to_forecast.month)
    ]
    return historical_same_day['Temp'].mean()

# Configuration of Data Nodes
historical_temperature_cfg = Config.configure_data_node("historical_temperature")
date_to_forecast_cfg = Config.configure_data_node("date_to_forecast")
predictions_cfg = Config.configure_data_node("predictions")

# Configuration of tasks
predictions_cfg = Config.configure_task("predict",
                                        predict,
                                        [historical_temperature_cfg, date_to_forecast_cfg],
                                        predictions_cfg)

# Configuration of scenario
scenario_cfg = Config.configure_scenario(id="my_scenario", task_configs=[predictions_cfg])

Config.export('config.toml')

if __name__ == '__main__':
    # Run of the Core
    tp.Core().run()

    # Creation of the scenario and execution
    scenario = tp.create_scenario(scenario_cfg)
    scenario.historical_temperature.write(data)
    scenario.date_to_forecast.write(dt.datetime.now())
    tp.submit(scenario)

    print("Value at the end of task", scenario.predictions.read())

    def save(state):
        state.scenario.historical_temperature.write(data)
        state.scenario.date_to_forecast.write(state.date)
        tp.gui.notify(state, "s", "Saved! Ready to submit")

    date = None
    scenario_md = """
<|{scenario}|scenario_selector|>

Put a Date
<|{date}|date|on_change=save|active={scenario}|>

Run the scenario
<|{scenario}|scenario|>
<|{scenario}|scenario_dag|>

View all the information on your prediction here
<|{scenario.predictions if scenario else None}|data_node|>
"""

    tp.Gui(scenario_md).run()
```